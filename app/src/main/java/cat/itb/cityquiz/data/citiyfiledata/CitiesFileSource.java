package cat.itb.cityquiz.data.citiyfiledata;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import cat.itb.cityquiz.domain.City;

public class CitiesFileSource {
    List<City> cities;

    public CitiesFileSource() {
        load();
    }

    private void load(){
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<City>>(){}.getType();
        cities = gson.fromJson(CityJson.json, listType);
    }

    public List<City> getCities() {
        return cities;
    }

    public List<City> getNRandomCities(int n, List<City> skipCities){
        List<City> selection = new ArrayList<>();
        List<City> availableCities = new ArrayList<>(cities);
        for(City city : skipCities){
            availableCities.remove(city);
        }
        Random random = new Random();
        for(int i=0; i<n; i++){
            int randomInt = random.nextInt(availableCities.size());
            selection.add(availableCities.remove(randomInt));
        }
        return selection;
    }


}
