package cat.itb.cityquiz.repository;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cat.itb.cityquiz.data.citiyfiledata.CitiesFileSource;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;

public class GameLogic {
    CitiesFileSource citiesFileSource;
    Random random = new Random();


    public GameLogic(CitiesFileSource citiesFileSource) {
        this.citiesFileSource = citiesFileSource;
    }

    public Game createGame(int numQuestions, int numAnswers){
        List<Question> questions = createQuestions(numQuestions, numAnswers);
        Game game = new Game(questions);
        return game;
    }

    public Game answerQuestions(Game game, int response){
        Question currentQuestion = game.getCurrentQuestion();
        City answer = currentQuestion.getPossibleCities().get(response);
        game.questionAnswered(answer);
        return game;
    }

    public Game skipQuestion(Game game){
        game.skipQuestion();
        return game;
    }


    private List<Question> createQuestions(int numQuestions, int numAnswers){
        List<Question> questions = new ArrayList<>();
        List<City> usedCities = new ArrayList<>();
        for(int i=0; i<numQuestions; ++i){
            Question question = createQuestion(numAnswers, usedCities);
            usedCities.add(question.getCorrectCity());
            questions.add(question);
        }
        return questions;
    }

    private Question createQuestion(int numAnswers, List<City> usedCities){
        List<City> answers = citiesFileSource.getNRandomCities(numAnswers, usedCities);
        City correct = answers.get(random.nextInt(answers.size()));
        Question question = new Question(correct, answers);
        return question;
    }

}
