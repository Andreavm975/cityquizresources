package cat.itb.cityquiz.presentation.ui.cityquiz.screens.result;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.presentation.ui.cityquiz.screens.start.QuizViewModel;

public class ScoreFragment extends Fragment {

    private QuizViewModel mViewModel;
    TextView tvScore;

    public static ScoreFragment newInstance() {
        return new ScoreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.score_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        // TODO: Use the ViewModel
        tvScore.setText(new StringBuilder().append("").append(mViewModel.getScore()).toString());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.againBtn).setOnClickListener(this::changeFragment);
        tvScore=view.findViewById(R.id.textView2);
    }

    private void changeFragment(View view) {
        mViewModel.createGame();
        Navigation.findNavController(view).navigate(R.id.score_to_question);
    }

}
