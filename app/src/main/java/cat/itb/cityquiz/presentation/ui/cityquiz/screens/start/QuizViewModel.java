package cat.itb.cityquiz.presentation.ui.cityquiz.screens.start;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class QuizViewModel extends ViewModel {
    private GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    private MutableLiveData<Game> game=new MutableLiveData<>();


    public LiveData<Game> getGame() {
        return game;
    }

    public void createGame() {
        game.postValue(gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers));
        //game=gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers);
    }

    public void answerCities(int option){
        game.postValue(gameLogic.answerQuestions(game.getValue(),option));
        //return gameLogic.answerQuestions(game, option);
    }

    public int getScore(){
        return game.getValue().getScore();
    }



}
