package cat.itb.cityquiz.presentation.ui.cityquiz.screens.start;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Question;

public class StartQuizFragment extends Fragment {

    private QuizViewModel mViewModel;

    public static StartQuizFragment newInstance() {
        return new StartQuizFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_quiz_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        // TODO: Use the ViewModel

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.startBtn).setOnClickListener(this::changeFragment);

    }

    private void changeFragment(View view) {
        mViewModel.createGame();
        Navigation.findNavController(view).navigate(R.id.start_to_question);

    }


}
