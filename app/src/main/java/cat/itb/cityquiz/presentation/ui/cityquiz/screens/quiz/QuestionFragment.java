package cat.itb.cityquiz.presentation.ui.cityquiz.screens.quiz;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.material.button.MaterialButton;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.ui.cityquiz.screens.start.QuizViewModel;
import cat.itb.cityquiz.repository.AnswerCountDownTimer;

public class QuestionFragment extends Fragment {

    private QuizViewModel mViewModel;
    ImageView imageView;
    MaterialButton answer1;
    MaterialButton answer2;
    MaterialButton answer3;
    MaterialButton answer4;
    MaterialButton answer5;
    MaterialButton answer6;
    AnswerCountDownTimer countDownTimer;
    ProgressBar progressBar;



    public static QuestionFragment newInstance() {
        return new QuestionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(QuizViewModel.class);
        // TODO: Use the ViewModel
        mViewModel.getGame().observe(this, this::onChanged);

    }

    private void onChanged(Game game) {
        if(game.isFinished()){
            Navigation.findNavController(getView()).navigate(R.id.question_to_score);
            countDownTimer.stop();
        }else {
            displayGame(game);
            progressBar.setProgress(10000);
            countDownTimer.start();
            countDownTimer.setTimerChangedListener(this::onProgressChanged);

        }

    }

    private void onProgressChanged(int i) {
        progressBar.setProgress(i);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setOnClickListener(this::changeToScoreFragment);

        imageView= view.findViewById(R.id.photoQuestion);
        answer1=view.findViewById(R.id.button1);
        answer2=view.findViewById(R.id.button2);
        answer3=view.findViewById(R.id.button3);
        answer4=view.findViewById(R.id.button4);
        answer5=view.findViewById(R.id.button5);
        answer6=view.findViewById(R.id.button6);
        progressBar=view.findViewById(R.id.progressBar);
        countDownTimer =new AnswerCountDownTimer(10000);

    }

    private void changeToScoreFragment(View view) {
        Navigation.findNavController(view).navigate(R.id.question_to_score);
    }

    private void displayGame(Game game){

        String fileName = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
        int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        imageView.setImageResource(resId);
        configureAnswerButton(game,answer1,0);
        configureAnswerButton(game,answer2,1);
        configureAnswerButton(game,answer3,2);
        configureAnswerButton(game,answer4,3);
        configureAnswerButton(game,answer5,4);
        configureAnswerButton(game,answer6,5);

    }

    private void configureAnswerButton(Game game, MaterialButton button, int i) {
        button.setText(game.getCurrentQuestion().getPossibleCities().get(i).getName());
        button.setTag(i);
        button.setOnClickListener(this::answerQuestion);
    }

    private void answerQuestion(View view){
        int answerTag= (Integer)view.getTag();
        //Game game=mViewModel.answerCities(answerTag);
        mViewModel.answerCities(answerTag);
//        if(game.isFinished()){
//            Navigation.findNavController(view).navigate(R.id.question_to_score);
//        }else {
//            displayGame(game);
//        }
    }

}
